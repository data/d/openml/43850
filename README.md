# OpenML dataset: Hatred-on-Twitter-During-MeToo-Movement

https://www.openml.org/d/43850

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Interest and Motivation
This dataset belongs to the MeToo movement on Twitter. This movement was against the sexual harassment incidents and many people posted various hatred tweets. Using this dataset, we can build a model that can accurately classify hatred and non-hatred tweets to restrict its spread.
Dataset Description
The details about the columns are as follows:

status_id: A unique id for each tweet [numeric].
text: tweet text data [string].
created_at: The timestamp of the tweet [timestamp].
favourite_count: favourite count of the user of the tweet [numeric].
retweet_count: retweet count of the tweet [numeric].
location: location mentioned by the user while tweeting [string].
followers_count: user's followers' count [numeric].
friends_count: user's friends' count [numeric].
statuses_count: user's total statuses count [numeric].
category: target variable, whether tweet belongs to hatred (category=1) or non-hatred (catogory=0).

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43850) of an [OpenML dataset](https://www.openml.org/d/43850). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43850/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43850/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43850/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

